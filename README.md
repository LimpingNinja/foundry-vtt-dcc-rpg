[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/LimpingNinja/foundry-vtt-dcc-rpg) 

# Foundry VTT DCC RPG

An implementation of the DCC RPG game system for Foundry Virtual Tabletop (http://foundryvtt.com). 